#include <LiquidCrystal.h>

LiquidCrystal lcd(7, 8, 9, 10, 11 , 12);
 int ventilador = 13; // PIN digital donde conectar el Ventilador
float c=0; //variable de la temperatura en centigrados

//Hasta aquí hemos declarado la librería para la LCD y los pines por donde le va a entrar la información.
// Funcion para leer el dato analogico y convertirlo a digital:
float centi()
{
//Esta fórmula sale de la relación del sensor con los grados.  
//El sensor de temperatura LM35 responde a variaciones de 10 mV por cada grado centígrado. Si el sensor detecta 1 grado centígrado a la salida del sensor obtendríamos 10 mV. Ejemplo: 26,4ºC = 264 mV = 0.264 V.
//Tenemos que el convertidor de analógico a digital es de 10 bits de resolución, los valores variarán entre 0 y 1023, entonces Vout= (5V*Dato)/1023 siendo  ( 0 < Dato < 1023 ) y para ajustar la escala a grados centígrados: Vout = ((5V*Dato)*100)/1023
c = (4.2 * analogRead(0)*100.0)/1023.0;
  return (c);
}

void setup() {
  Serial.begin (9600); //inicia comunicacion serial
  pinMode(ventilador, OUTPUT); // Inicializa el pin 13 como salida digital
  // Definimos la LCD con dimension 2×16 y definimos los caracteres que deben salir en las filas:
  lcd.begin(16,2);
  lcd.clear(); //limpiarlo porque puede quedarse pegado valores que ya tenia si volvemos a compilar
  lcd.print("C=");
  lcd.setCursor(0,1);
  lcd.print("Temperatura");
}

//Hasta aquí hemos definido qué queremos que salga impreso por la pantalla y el tamaño de ésta.
void loop() {
  float Centigrados = centi();
  Serial.println (Centigrados); //escribe la temperatura en el serial
  lcd.setCursor(2,0); //posiciona en el columna dos de la fila 0 (para mostrar temperatura)
  lcd.print(Centigrados);
  delay (500); //espera medio segundo

//esto enciende y apaga el ventlador
if (Centigrados < 24){//cambiar numero en funcion de lo que se desee
  digitalWrite(13, LOW);
  
  lcd.setCursor(0,1);
  lcd.print("--Temperatura---");
}else{ //si no es menor a 24 enciende ventilador y actualiza LCD
  digitalWrite(13,HIGH);
  //Escribimos el mensaje en el LCD
  lcd.setCursor(0,1);
  lcd.print("Caliente,Enfria!");
  //Escribimos en el serial
  Serial.println("CALIENTEEE!!! ENFRIANDO!!!. \n");
}
}
